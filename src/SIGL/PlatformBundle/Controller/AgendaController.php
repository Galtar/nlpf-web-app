<?php

namespace SIGL\PlatformBundle\Controller;

use Google_Client;
use Google_Service_Calendar;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AgendaController extends Controller
{
    public function testAction(Request $request)
    {

        $redirectUri = "/agenda";
        $googleCalendar = $this->get('fungio.google_calendar');
        $pathToCredentials = __DIR__.'/../../../../app/Resources/GoogleCalendarBundle/credentials.json';
        $pathToClientSecret = __DIR__.'/../../../../app/Resources/GoogleCalendarBundle/client_secret.json';
        $googleCalendar->setApplicationName('NLPF');
        //$googleCalendar->setCredentialsPath("$pathToCredentials");
        $googleCalendar->setClientSecretPath("$pathToCredentials");
        $googleCalendar->setAccessToken("4/uABM8HinotWbUkTalViEpOFIfuYLVdnux4Wx0JXfTHn7WNoot7S8DBk");
        if ($request->query->has('code') && $request->get('code')) {
            $client = $googleCalendar->getClient($request->get('code'));
        } else {
            $client = $googleCalendar->getClient();
        }
        $googleCalendar->setRedirectUri($redirectUri);
        if (is_string($client)) {
            return new RedirectResponse($client);
        }

        $events = $googleCalendar->getEventsForDate('primary', new \DateTime('now'));

    }
}
