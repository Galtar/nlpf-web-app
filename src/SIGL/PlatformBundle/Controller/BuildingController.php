<?php

namespace SIGL\PlatformBundle\Controller;

use SIGL\PlatformBundle\Entity\Building;
use SIGL\PlatformBundle\Form\BuildingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class BuildingController extends Controller
{
    /**
     * @param Request $request
     * @param $idUser
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function createAction(Request $request, $idUser)
    {
        $building = new Building();
        $formBuilding = $this->createForm(BuildingType::class, $building);

        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $formBuilding->handleRequest($request);

            if ($formBuilding->isValid()) {
                $building->setUser($em->getRepository('SIGL\PlatformBundle\Entity\User')->find($this->getUser()->getId()));

                $user = $em->getRepository('SIGL\PlatformBundle\Entity\User')->find($this->getUser()->getId());
                $user->addRole('ROLE_ADMIN');
                $em->persist($building);
                $em->flush();

                $this->addFlash("success", 'Votre building a été ajouté avec succès.');

                if ($this->get('security.authorization_checker')->isGranted('ROLE_USER') and !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
                    return $this->redirectToRoute('logout');
                return $this->redirectToRoute('sigl_platform_account_home');
            }
            else
                $em->remove($em->getRepository('SIGL\PlatformBundle\Entity\User')->find($idUser));
        }

        return $this->render('SIGLPlatformBundle:Platform:addBuilding.html.twig', array(
            'formBuilding' => $formBuilding->createView(),
        ));
    }
}
