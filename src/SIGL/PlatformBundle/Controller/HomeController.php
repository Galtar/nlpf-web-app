<?php

namespace SIGL\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class HomeController extends Controller
{
    public function indexAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();
            $buildings = $em->getRepository('SIGL\PlatformBundle\Entity\Building')->findBy(array('user' => $user));
            if (count($buildings) > 0)
                return $this->render('SIGLPlatformBundle:Platform:index.html.twig');
            else {
                return $this->redirectToRoute('sigl_platform_building_add', array(
                    'idUser' => $user->getId()
                ));
            }

        }

        return $this->render('SIGLPlatformBundle:Platform:index.html.twig');
    }
}
