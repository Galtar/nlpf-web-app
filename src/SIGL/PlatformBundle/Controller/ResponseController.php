<?php

namespace SIGL\PlatformBundle\Controller;

use SIGL\PlatformBundle\Entity\Response;
use SIGL\PlatformBundle\Entity\User;
use SIGL\PlatformBundle\Form\ResponseType;
use SIGL\PlatformBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ResponseController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     */
    public function indexAction()
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        return $this->render('SIGLPlatformBundle:Platform:createAccount.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $response = new Response();

        $form = $this->createForm(ResponseType::class, $response, array(
            'action' => null,
            'method' => 'POST',
            'user' => $user
        ));

        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $response->setUser($user);
                $response->setState('en attente');

                $em->persist($response);
                $em->flush();

                $this->addFlash('success', 'Ticket bien enregistrée');

                return $this->redirectToRoute('sigl_platform_ticket', array('id' => $response->getId()));
            }
        }

        return $this->render('SIGLPlatformBundle:Platform:createTicket.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
