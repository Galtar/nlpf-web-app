<?php

namespace SIGL\PlatformBundle\Controller;

use SIGL\PlatformBundle\Entity\Response;
use SIGL\PlatformBundle\Entity\Ticket;
use SIGL\PlatformBundle\Form\ResponseRefuseType;
use SIGL\PlatformBundle\Form\ResponseType;
use SIGL\PlatformBundle\Form\TicketType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class TicketController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function indexAdminAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tickets = $em->getRepository('SIGL\PlatformBundle\Entity\Ticket')->findAll();

        return $this->render('SIGLPlatformBundle:Platform:ticketsAdmin.html.twig', array(
            'tickets' => $tickets
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tickets = $em->getRepository('SIGL\PlatformBundle\Entity\Ticket')->findBy(
            array('user' => $em->getRepository('SIGL\PlatformBundle\Entity\User')->find($this->getUser()->getId()))
        );

        return $this->render('SIGLPlatformBundle:Platform:tickets.html.twig', array(
            'tickets' => $tickets
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function ticketAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $ticket = $em->getRepository('SIGL\PlatformBundle\Entity\Ticket')->find($id);

        $response = $em->getRepository('SIGL\PlatformBundle\Entity\Response')->findBy(array('ticket' => $ticket));

        if (count($response) > 0)
            $response[0]->setNotif(false);

        $em->flush();

        return $this->render('SIGLPlatformBundle:Platform:ticket.html.twig', array(
            'ticket' => $ticket
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function ticketAdminAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $ticket = $em->getRepository('SIGL\PlatformBundle\Entity\Ticket')->find($id);

        return $this->render('SIGLPlatformBundle:Platform:ticketAdmin.html.twig', array(
            'ticket' => $ticket
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $ticket = new Ticket();

        $form = $this->createForm(TicketType::class, $ticket, array(
            'action' => null,
            'method' => 'POST',
            'user' => $user
        ));

        if ($request->isMethod('POST')) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $ticket->setUser($user);
                $ticket->setState('en attente');

                $em->persist($ticket);
                $em->flush();

                $this->addFlash('success', 'Ticket bien enregistrée');

                return $this->redirectToRoute('sigl_platform_ticket', array('id' => $ticket->getId()));
            }
        }

        return $this->render('SIGLPlatformBundle:Platform:createTicket.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_SUPER_ADMIN')")
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $ticket = $em->getRepository('SIGL\PlatformBundle\Entity\Ticket')->find($id);
        $form = $this->createForm(TicketType::class, $ticket, array(
            'action' => null,
            'method' => 'POST',
            'user' => $user
        ));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em->flush();

                $this->addFlash("success", 'Votre ticket a été modifié avec succès.');

                return $this->redirectToRoute('sigl_platform_tickets');
            }
        }
        return $this->render('SIGLPlatformBundle:Platform:updateTicket.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_SUPER_ADMIN')")
     */
    public function deleteAction($id)
    {
        return $this->redirectToRoute('sigl_platform_homepage');
    }

    /**
     * @param Request $request
     * @param $id
     * @param $state
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_SUPER_ADMIN')")
     */
    public function changeStateAction(Request $request, $id, $state)
    {
        $em = $this->getDoctrine()->getManager();
        $ticket = $em->getRepository('SIGL\PlatformBundle\Entity\Ticket')->find($id);

        $statePrevious = $ticket->getState();

        $ticket->setState($state);
        $em->flush();

        if ($statePrevious == 'refuse' and  $state == 'en attente') {
            $em->remove($em->getRepository('SIGL\PlatformBundle\Entity\Response')->findBy(array('ticket' => $ticket))[0]);
            $em->flush();
            return $this->redirectToRoute('sigl_platform_ticket_edit', array(
                'id' => $id
            ));
        }
        $response = new Response();
        if ($state != 'accepte' and $state != 'refuse') {
            return $this->redirectToRoute('sigl_platform_admin_ticket', array(
                'id' => $id
            ));
        }
        if ($state == 'accepte')
            $form = $this->createForm(ResponseType::class, $response);
        if ($state == 'refuse')
            $form = $this->createForm(ResponseRefuseType::class, $response);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $response->setTicket($ticket);
                $response->setNotif(1);
                if ($state == 'accepte') {
                    $response->setAccepted(1);
                    $response->setJustification("");
                }
                else {
                    $response->setAccepted(0);
                    $response->setDate(new \DateTime('00-00-1900 00:00'));
                }
                $em->persist($response);
                $em->flush();

                $this->addFlash('success', 'Réponse bien enregistrée');

                return $this->redirectToRoute('sigl_platform_admin_ticket', array('id' => $id));
            }
        }

        return $this->render('SIGLPlatformBundle:Platform:addResponse.html.twig', array(
            'form' => $form->createView(),
            'state' => $state
        ));
    }
}
