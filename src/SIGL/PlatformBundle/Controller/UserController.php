<?php

namespace SIGL\PlatformBundle\Controller;

use SIGL\PlatformBundle\Entity\Building;
use SIGL\PlatformBundle\Entity\User;
use SIGL\PlatformBundle\Form\BuildingType;
use SIGL\PlatformBundle\Form\LoginType;
use SIGL\PlatformBundle\Form\UserType;
use SIGL\PlatformBundle\Form\UserUpdateType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = new User();
        $formUser = $this->createForm(UserType::class, $user);

        if ($request->isMethod('POST')) {
            $formUser->handleRequest($request);

            if ($formUser->isValid()) {
                $user->setUsername($formUser->getData()->getEmail());
                $user->setSalt('');
                $user->setBlacklist(false);
                $user->addRole('ROLE_USER');

                $em->persist($user);
                $em->flush();

                $this->addFlash("success", 'Votre compte utilisateur a été crée avec succès.');

                return $this->redirectToRoute('sigl_platform_building_add', array('idUser' => $user->getId()));
            }
        }
        return $this->render('SIGLPlatformBundle:Platform:createAccount.html.twig', array(
            'formUser' => $formUser->createView(),
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_SUPER_ADMIN')")
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('SIGL\PlatformBundle\Entity\User')->find($this->getUser()->getId());
        $formUser = $this->createForm(UserUpdateType::class, $user);

        if ($request->isMethod('POST')) {
            $formUser->handleRequest($request);

            if ($formUser->isValid()) {
                $em->flush();

                $this->addFlash("success", 'Votre compte utilisateur a été modifié avec succès.');

                return $this->redirectToRoute('sigl_platform_account_home');
            }
        }
        return $this->render('SIGLPlatformBundle:Platform:updateAccount.html.twig', array(
            'formUser' => $formUser->createView(),
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        return $this->render('SIGLPlatformBundle:Platform:login.html.twig', array(
            'last_username' => $authenticationUtils->getLastUsername(),
            'error'         => $authenticationUtils->getLastAuthenticationError(),
        ));

        /*
        $formLogin = $this->createForm(LoginType::class);
        if ($request->isMethod('POST')) {
            $formLogin->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            $repositoryUsers = $em->getRepository('SIGL\PlatformBundle\Entity\User');

            if ($formLogin->isValid()) {
                $user = $repositoryUsers->findBy(array(
                    'mail' => $formLogin->getData()['email'],
                    'password' => $formLogin->getData()['password']
                ));

                if (count($user)) {
                    $this->addFlash("success", 'Connexion réussie.');
                    return $this->redirectToRoute('sigl_platform_account_home');
                }
                else
                    $this->addFlash("error", 'Veuillez vérifier vos informations.');
            }
        }
        return $this->render('SIGLPlatformBundle:Platform:login.html.twig', array(
            'formLogin' => $formLogin->createView(),
        ));
        */
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_SUPER_ADMIN')")
     */
    public function homeAction()
    {
        return $this->render('SIGLPlatformBundle:Platform:profil.html.twig', array(
            'user' => $this->getUser()
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function customersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $allCustomers = $em->getRepository('SIGL\PlatformBundle\Entity\User')->findAll();

        return $this->render('SIGLPlatformBundle:Platform:customers.html.twig', array(
            'customers' => $allCustomers
        ));
    }

    /**
     * @param $id
     * @param $state
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function blacklistAction($id, $state)
    {
        $em = $this->getDoctrine()->getManager();
        $customer = $em->getRepository('SIGL\PlatformBundle\Entity\User')->find($id);

        $customer->setBlacklist($state);

        $em->flush();

        $allCustomers = $em->getRepository('SIGL\PlatformBundle\Entity\User')->findAll();

        $customers = array();
        foreach ($allCustomers as $customer) {
            if (!empty($customer->getTickets()))
                $customers[] = $customer;
        }
        return $this->redirectToRoute('sigl_platform_customers', array(
            'customers' => $customers
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function notificationAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tickets = $em->getRepository('SIGL\PlatformBundle\Entity\Ticket')->findBy(array('user' => $this->getUser()));

        $notifications = array();
        foreach ($tickets as $ticket) {
            $notifications[] = $em->getRepository('SIGL\PlatformBundle\Entity\Response')->findBy(array('ticket' => $ticket));
        }

        $notifs = array();
        foreach ($notifications as $notification) {
            if (count($notification) > 0) {
                if ($notification[0]->getNotif())
                    $notifs[] = $notification;
            }
        }

        return $this->render('SIGLPlatformBundle:Platform:notifs.html.twig', array(
            'notifs' => $notifs,
        ));
    }
}