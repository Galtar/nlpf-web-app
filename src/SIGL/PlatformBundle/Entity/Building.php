<?php

namespace SIGL\PlatformBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="SIGL\PlatformBundle\Entity\BuildingRepository")
 * @ORM\Table(name="buildings")
 */
class Building
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $address;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="buildings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Ticket", mappedBy="building")
     */
    protected $tickets;


    public function __construct()
    {
        $this->tickets = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->address;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $user->addBuilding($this);
        $this->user = $user;
    }

    public function addTicket($ticket)
    {
        $this->tickets[] = $ticket;
    }
}
