<?php

namespace SIGL\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="SIGL\PlatformBundle\Entity\ResponseRepository")
 * @ORM\Table(name="responses")
 */
class Response
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var boolean
     */
    protected $accepted;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var datetime
     */
    protected $date;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $justification;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var boolean
     */
    protected $notif;

    /**
     * @ORM\OneToOne(targetEntity="Ticket", inversedBy="response")
     * @ORM\JoinColumn(name="ticket_id", referencedColumnName="id")
     */
    protected $ticket;


    public function getId()
    {
        return $this->id;
    }

    public function getAccepted()
    {
        return $this->accepted;
    }

    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getJustification()
    {
        return $this->justification;
    }

    public function setJustification($justification)
    {
        $this->justification = $justification;
    }

    public function getNotif()
    {
        return $this->notif;
    }

    public function setNotif($notif)
    {
        $this->notif = $notif;
    }

    public function getTicket()
    {
        return $this->ticket;
    }

    public function setTicket($ticket)
    {
        $ticket->setResponse($this);
        $this->ticket = $ticket;
    }
}
