<?php

namespace SIGL\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="SIGL\PlatformBundle\Entity\TicketRepository")
 * @ORM\Table(name="tickets")
 */
class Ticket
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    protected $id;

    /*
    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    /*
    protected $photo;
    */

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    protected $etage;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int (ENUM(16))
     */
    protected $orientation;

    /**
     * @ORM\Column(type="string")
     *
     * @var string (ENUM(4))
     */
    protected $state;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tickets")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Building", inversedBy="tickets")
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id")
     */
    protected $building;

    /**
     * @ORM\OneToOne(targetEntity="Response", mappedBy="ticket")
     */
    protected $response;


    public function getId()
    {
        return $this->id;
    }

    /*
    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }
    */

    public function getEtage()
    {
        return $this->etage;
    }

    public function setEtage($etage)
    {
        $this->etage = $etage;
    }

    public function getOrientation()
    {
        return $this->orientation;
    }

    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $user->addTicket($this);
        $this->user = $user;
    }

    public function getBuilding()
    {
        return $this->building;
    }

    public function setBuilding(Building $building)
    {
        $building->addTicket($this);
        $this->building = $building;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function setResponse($response)
    {
        $this->response = $response;
    }
}
