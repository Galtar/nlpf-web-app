<?php

namespace SIGL\PlatformBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="SIGL\PlatformBundle\Entity\UserRepository")
 * @ORM\Table(name="users")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", unique=true)
     *
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $password;

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @ORM\Column(type="string", unique=true)
     *
     * @var string
     */
    protected $phone;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $address;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var boolean
     */
    protected $blacklist;

    /**
     * @ORM\Column(name="roles", type="array")
     */
    protected $roles = array();

    /**
     * @ORM\OneToMany(targetEntity="Building", mappedBy="user")
     */
    protected $buildings;

    /**
     * @ORM\OneToMany(targetEntity="Ticket", mappedBy="user")
     */
    protected $tickets;


    public function __construct()
    {
        $this->buildings = new ArrayCollection();
        $this->tickets = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getFirstName()
    {
        return $this->firstname;
    }

    public function setFirstName($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getLastName()
    {
        return $this->lastname;
    }

    public function setLastName($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    public function getAdmin()
    {
        return $this->admin;
    }

    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    public function getBlacklist()
    {
        return $this->blacklist;
    }

    public function setBlacklist($blacklist)
    {
        $this->blacklist = $blacklist;
    }

    public function addBuilding($building)
    {
        $this->buildings[] = $building;
    }

    public function getBuildings()
    {
        return $this->buildings;
    }

    public function addTicket($ticket)
    {
        $this->tickets[] = $ticket;
    }

    public function getTickets()
    {
        return $this->tickets;
    }

    public function addRole($role)
    {
        $this->roles[] = $role;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {
    }
}
