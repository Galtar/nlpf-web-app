<?php
// src/Form/TicketType.php
namespace SIGL\PlatformBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class TicketType extends AbstractType
{
    private $user;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $options['user'];
        $builder
            //->add('photo', FileType::class)
            ->add('etage', IntegerType::class, array('attr' => array('min' => 0), 'label' => 'Etage'))
            ->add('orientation', ChoiceType::class, array(
                'choices' => array(
                    'Nord' => 0,
                    'Nord-Nord-Est' => 1,
                    'Nord-Est' => 2,
                    'Est-Nord-Est' => 3,
                    'Est' => 4,
                    'Est-Sud-Est' => 5,
                    'Sud-Est' => 6,
                    'Sud-Sud-Est' => 7,
                    'Sud' => 8,
                    'Sud-Sud-Ouest' => 9,
                    'Sud-Ouest' => 10,
                    'Ouest-Sud-Ouest' => 11,
                    'Ouest' => 12,
                    'Ouest-Nord-Ouest' => 13,
                    'Nord-Ouest' => 14,
                    'Nord-Nord-Ouest' => 15
                ),
                'label' => 'Orientation'
            ))
            ->add('building', EntityType::class, array(
                'class' => 'SIGL\PlatformBundle\Entity\Building',
                'query_builder' => function (EntityRepository $entityRepository) {
                    $buildingsquery = $entityRepository
                        ->createQueryBuilder('b')
                        ->where('b.user = :user')
                        ->setParameter('user', $this->user);
                    return $buildingsquery;
                },
                'label' => 'Bâtiment'
            ))
            ->add('save', SubmitType::class, array('label' => 'Créer/Mettre à jour le ticket'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SIGL\PlatformBundle\Entity\Ticket',
            'user' => null,
        ));
    }
}